var del = require('del');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var combiner = require('stream-combiner2');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

global.isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

var paths = new (function() {
  this.src = 'src';
  this.dest = 'dist';
  this.styles = {
    src: this.src + ['/styles/*.styl'],
    watch: this.src + '/styles/**/*.styl',
    dest: this.dest + '/css',
    vendor: [
      'node_modules/normalize.css/normalize.css'
    ]
  };
  this.jade = {
    src: this.src + '/jade/*.jade',
    watch: this.src + '/jade/**/*.jade',
    dest: this.dest
  };
  this.js = {
    src: this.src + ['/js/*.js'],
    watch: this.src + '/js/*.js',
    dest: this.dest + '/js',
    vendor: []
  };
  this.assets = {
    src: this.src + '/assets/**/*.*',
    watch: this.src + '/assets/**/*.*',
    dest: this.dest + '/assets'
  };
})();

gulp.task('jade', function() {
  combiner(
    gulp.src(paths.jade.src),
    $.jade({pretty: true}),
    gulp.dest(paths.jade.dest),
    reload({stream: true})
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'Jade',
      message: err.message
    };
  }));
});

gulp.task('styles', function() {
  combiner(
    gulp.src(paths.styles.src),
    $.if(isDevelopment, $.sourcemaps.init()),
    $.stylus(),
    $.autoprefixer(),
    $.if(isDevelopment, $.concat('style.css'), $.concat('style.min.css')),
    $.if(isDevelopment, $.sourcemaps.write(), $.cssnano()),
    gulp.dest(paths.styles.dest),
    reload({stream: true})
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'Styles',
      message: err.message
    };
  }));
});

gulp.task('styles:vendor', function() {
  combiner(
    gulp.src(paths.styles.vendor),
    $.concat('vendor.min.css'),
    $.cssnano(),
    gulp.dest(paths.styles.dest)
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'Styles Vendor',
      message: err.message
    }
  }));
});

gulp.task('js', function() {
  combiner(
    gulp.src(paths.js.src),
    $.if(isDevelopment, $.concat('all.js'), $.concat('all.min.js')),
    $.if(!isDevelopment, $.uglify()),
    gulp.dest(paths.js.dest),
    reload({stream: true})
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'JavaScript',
      message: err.message
    };
  }));
});

gulp.task('js:vendor', function() {
  combiner(
    gulp.src(paths.js.vendor),
    $.concat('vendor.min.js'),
    $.uglify(),
    gulp.dest(paths.js.dest)
  ).on('error', $.notify.onError(function(err) {
    return {
      title: 'JS Vendor',
      message: err.message
    }
  }));
});

gulp.task('serve', function() {
  browserSync.init({
    server: paths.dest,
    notify: false
  });
});

gulp.task('assets', function() {
  gulp.src(paths.assets.src)
  .pipe($.newer(paths.assets.dest))
  .pipe(gulp.dest(paths.assets.dest));
});

gulp.task('watch', function() {
  gulp.watch(paths.js.watch, ['js']);
  gulp.watch(paths.jade.watch, ['jade']);
  gulp.watch(paths.styles.watch, ['styles']);
  gulp.watch(paths.assets.watch, ['assets']);
});

gulp.task('clean', function() {
  del(paths.dest);
});

gulp.task('default', [
  'styles',
  'jade',
  'js',
  'styles:vendor',
  'js:vendor',
  'assets',
  'watch',
  'serve'
]);